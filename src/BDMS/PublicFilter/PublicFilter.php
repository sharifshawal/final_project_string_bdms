<?php
namespace App\PublicFilter;

use App\Model\Database as DB;
use PDO;

class PublicFilter extends DB{

    public $location = "";
    public $blood_group = "";

    public function __construct(){
        parent::__construct();
    }

    public function setData($data=array()){

        if(array_key_exists('blood_group',$data)){
            $this->blood_group=$data['blood_group'];
        }

        if(array_key_exists('location',$data)){
            $this->location=$data['location'];
        }

    }


    public function filterData(){
        $sql="SELECT first_name, last_name, location, blood_group FROM `bdms_user` WHERE `bdms_user`.location=:location AND `bdms_user`.blood_group=:blood_group";
        $result=$this->DBH->prepare($sql);
        $result->execute(array(':location'=>$this->location, ':blood_group'=>$this->blood_group));
        $allData=$result->fetchAll(PDO::FETCH_OBJ);
        return $allData;
    }// end of filterData()


    public function ShowNotice(){

    }

    public function indexPaginator($page=1,$itemsPerPage=10){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT first_name, last_name, location, blood_group FROM `bdms_user` WHERE `bdms_user`.location=:location AND `bdms_user`.blood_group=:blood_group LIMIT $start,$itemsPerPage";

        $result= $this->DBH->prepare($sql);
        $result->execute(array(':location'=>$this->location, ':blood_group'=>$this->blood_group));

        $result->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $result->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();



}