<?php
namespace App\Model;
use PDO;
use PDOException;

class Database{
    public $DBH;
    public $username="root";
    public $password="";

    public function __construct()
    {
        try {

            # MySQL with PDO_MYSQL
            $this->DBH = new PDO("mysql:host=localhost;dbname=bdms_database", $this->username, $this->password);
			$this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }
}

