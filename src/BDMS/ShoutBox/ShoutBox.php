<?php
namespace App\ShoutBox;

use App\Model\Database as DB;
use PDO;

class ShoutBox extends DB
{
    public $user_id;
    public $message;
    public $post_time;
    public $post_date;
    public $status;

    public $blood_group;
    public $mobile_number;
    public $location;

        public function __construct()
        {
            parent::__construct();
        }

    public function setData($postData=array()){
        if(array_key_exists('display_name',$postData)){
            $this->user_id = $postData['display_name'];
        }
        if(array_key_exists('mobile_number',$postData)){
            $this->mobile_number ="+88".$postData['mobile_number'];
        }
        if(array_key_exists('location',$postData)){
            $this->location = $postData['location'];
        }
        if(array_key_exists('blood_group',$postData)){
            $this->blood_group = $postData['blood_group'];
        }

        date_default_timezone_set('Asia/Dhaka');
        $this->post_time = date('h:i:s:', time());
        $this->post_date= date('Y-m-d');

    }

    public function createMsg(){

        $this->message = $this->user_id." ( ".$this->mobile_number." ) from ".$this->location." needs ".$this->blood_group." Blood emergency";
        echo $this->message;
        return $this->message;
    }

    public function storeData(){
        $arrData=array($this->user_id, $this->message, $this->post_date, $this->post_time);
        
        //var_dump($this->user_id);die;
        $sql ="INSERT INTO `shoutbox`(`user_id`, `message`, `post_date`, `post_time`) VALUES (?, ?, ?, ?)";
        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
        header("Location:dashboard.php");
    }


/*    public function storeData()
    {
        $arrData = array(':user_id'=>$this->user_id, ':message'=>$this->message, ':post_date'=>$this->post_date, ':post_time' =>$this->post_time);
        $sql ="INSERT INTO `shoutbox`(`user_id`, `mesaage`, `post_date`, `post_time`) VALUES(:user_id, :message, :post_date, :post_time)";
        $STH = $this->DBH->prepare($sql);
        $STH->execute(array(':user_id'=>$this->user_id, ':message'=>$this->message, ':post_date'=>$this->post_date, ':post_time'=>$this->post_time));
        var_dump($STH);
    }*/

    public function getData($fetchMode='OBJ'){
        $sql="SELECT * from shoutbox WHERE status=1 ORDER BY id DESC ";
        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;

    }// end of index();
}