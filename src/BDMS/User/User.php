<?php
namespace App\User;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;


class User extends DB{

    public $table="bdms_user";

    public $id="";
    public $first_name="";
    public $last_name="";
    public $gender="";
    public $birthdate="";
    public $blood_group="";
    public $last_donate_date="";
    public $location="";
    public $address="";
    public $email="";
    public $email_verified="";
    public $mobile_number="";
    public $user_id="";
    public $password="";
    public $facebook_id="";
    public $number_visibilty="";
    public $status="";

    public $email_token="";


    public function __construct(){
        parent::__construct();
    }

    public function setData($data=array()){

        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }

        if(array_key_exists('first_name',$data)){
            $this->first_name=$data['first_name'];
        }
        if(array_key_exists('last_name',$data)){
            $this->last_name=$data['last_name'];
        }
        if(array_key_exists('gender',$data)){
            $this->gender=$data['gender'];
        }
        if(array_key_exists('birthdate',$data)){
            $this->birthdate=$data['birthdate'];
        }
        if(array_key_exists('blood_group',$data)){
            $this->blood_group=$data['blood_group'];
        }
        if(array_key_exists('last_donate_date',$data)){
            $this->last_donate_date=$data['last_donate_date'];
        }
        if(array_key_exists('location',$data)){
            $this->location=$data['location'];
        }
        if(array_key_exists('address',$data)){
            $this->address=$data['address'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('email_token',$data)){
            $this->email_verified=$data['email_token'];
        }
        if(array_key_exists('mobile_number',$data)){
            $this->mobile_number=$data['mobile_number'];
        }

        if(array_key_exists('username',$data)){
            $this->user_id=$data['username'];
        }

        if(array_key_exists('password',$data)){
            $this->password=($data['password']);
        }
        if(array_key_exists('facebook_id',$data)){
            $this->facebook_id=$data['facebook_id'];
        }
        if(array_key_exists('number_visibilty',$data)){
            $this->number_visibilty=$data['number_visibilty'];
        }

        if(array_key_exists('status',$data)){
            $this->status=$data['status'];
        }

        /*        echo "<pre>";
                var_dump($this->first_name);
                var_dump($this->last_name);
                var_dump($this->gender);
                var_dump($this->birthdate);
                var_dump($this->blood_group);
                var_dump($this->last_donate_date);
                var_dump($this->location);
                var_dump($this->address);
                var_dump($this->email);
                var_dump($this->email_verified);
                var_dump($this->mobile_number);
                var_dump($this->user_id);
                var_dump($this->password);
                var_dump($this->facebook_id);
                var_dump($this->number_visibilty);
                die();*/

    }


    public function store(){

        $arrData = array(
            $this->first_name,
            $this->last_name,
            $this->gender,
            $this->birthdate,
            $this->blood_group,
            $this->last_donate_date,
            $this->location,
            $this->address,
            $this->email,
            $this->email_verified,
            $this->mobile_number,
            $this->user_id,
            $this->password,
            $this->facebook_id,
            $this->number_visibilty,

        );

        /*        echo "<pre>";
                print_r($arrData);die;*/

        $sql = "INSERT INTO `bdms_database`.`bdms_user` ( `first_name`, `last_name`, `gender`,
  `birthdate`, `blood_group`, `last_donate_date`, `location`, `address`, `email`, `email_verified`,
   `mobile_number`, `user_id`, `password`, `facebook_id`, `number_visibilty`)
    VALUES (?, ?, ?, ?, ?, ?, ?, ?,
     ?, ?, ?, ?, ?, ?, ?)";
        $STH = $this->DBH->prepare($sql);

        $STH->execute($arrData);






        /*    public function store() {

                $query="INSERT INTO `bdms_database`.`bdms_user`

         (`first_name`, `last_name`, `gender`, `birthdate`, `blood_group`, `last_donate_date`, `location`, `address`,
         `email`, `mobile_number`, `password`, `facebook_id`,`email_verified`)
        VALUES (:firstName, :lastName, :gender, :birthDate, :bloodGroup, :lastDonateDate, :location, :address,
        :email, :phoneNumber, :password, :facebookId :email_token)";

                $result=$this->DBH->prepare($query);

                $result->bindParam(":firstName",$this->first_name);
                $result->bindParam(":lastName",$this->last_name);
                $result->bindParam(":gender",$this->gender);
                $result->bindParam(":birthDate",$this->birthdate);
                $result->bindParam(":bloodGroup",$this->blood_group);
                $result->bindParam(":lastDonateDate",$this->last_donate_date);
                $result->bindParam(":location",$this->location);
                $result->bindParam(":address",$this->address);
                $result->bindParam(":email",$this->email);
                $result->bindParam(":mobileNumber",$this->mobile_number);
                $result->bindParam(":password",$this->password);
                $result->bindParam(":facebookId",$this->facebook_id);
                $result->bindParam(":email_token",$this->email_token);

                        echo "<pre>";
                var_dump($this->first_name);
                var_dump($this->last_name);
                var_dump($this->gender);
                var_dump($this->birthdate);
                var_dump($this->blood_group);
                var_dump($this->last_donate_date);
                var_dump($this->location);
                var_dump($this->address);
                var_dump($this->email);
                var_dump($this->email_token);
                var_dump($this->mobile_number);
                var_dump($this->password);
                var_dump($this->facebook_id);*/

        if ($STH) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully, Please check your email and active your account.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully!!.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function change_password(){
        $query="UPDATE `bdms_database`.`bdms_user` SET `password`=:password  WHERE `bdms_user`.`email` =:email";
        $result=$this->DBH->prepare($query);
        $result->execute(array(':password'=>$this->password,':email'=>$this->email));

        if($result){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Password has been updated  successfully.
              </div>");
            //                                                                                                                  Utility::redirect('../../../../views/User/Profile/signUp.php');
        }
        else {
            echo "Error";
        }

    }

    public function view(){
        $query="SELECT * FROM `bdms_user` WHERE `bdms_user`.`email` =:email";
        $result=$this->DBH->prepare($query);
        $result->execute(array(':email'=>$this->email));
        $allData=$result->fetch(PDO::FETCH_OBJ);
        return $allData;
    }// end of view()


    public function validTokenUpdate(){
        $query="UPDATE `bdms_database`.`bdms_user` SET  `email_verified`='".'Yes'."' WHERE `bdms_user`.`email` =:email";
        $result=$this->DBH->prepare($query);
        $result->execute(array(':email'=>$this->email));

        if($result){
            Message::message("
             <div class=\"alert alert-success\" data-dismiss=\"alert\">
             <strong>Success!</strong> Email verification has been successful. Please login now!
              </div>");
        }
        else {
            echo "Error";
        }
        return Utility::redirect('../../../views/index.php');
    }

    public function update(){

        $arrData = array(
            $this->first_name,
            $this->last_name,
            $this->gender,
            $this->birthdate,
            $this->blood_group,
            $this->last_donate_date,
            $this->location,
            $this->address,
            $this->mobile_number,
            $this->facebook_id,
            $this->number_visibilty,
            $this->email);


        $query="UPDATE `bdms_user` SET `first_name` = ?, `last_name`= ?, `gender`= ?, `birthdate` = ?, `blood_group`= ?, `last_donate_date` = ?, `location` = ?, `address` = ?, `mobile_number` = ?, `facebook_id` = ?, `number_visibilty` = ? WHERE `email` = ?";
        $result=$this->DBH->prepare($query);

        $result->execute($arrData);

        if($result){
            Message::message("
             <div class=\"alert alert-info\" data-dismiss=\"alert\">
             <strong>Success!</strong> Data has been updated  successfully.
              </div>");
        }
        else {
            echo "Error";
        }
        return Utility::redirect($_SERVER['HTTP_REFERER']);
    }

    //////////////////////////////////
    ////////// Filter
    /////////        Method For User
    //////////////////////////////////

    public function filterData(){
        $sql="SELECT first_name, last_name, location, blood_group, mobile_number, email, number_visibilty FROM `bdms_user` WHERE `bdms_user`.location=:location AND `bdms_user`.blood_group=:blood_group";
        $result=$this->DBH->prepare($sql);
        $result->execute(array(':location'=>$this->location, ':blood_group'=>$this->blood_group));
        $allData=$result->fetchAll(PDO::FETCH_OBJ);
        return $allData;
    }// end of filterData()



}

