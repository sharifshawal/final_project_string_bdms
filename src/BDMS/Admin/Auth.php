<?php
namespace App\Admin;
if(!isset($_SESSION) )  session_start();
use App\Model\Database as DB;

class Auth extends DB{

    public $email = "";
    public $password = "";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postData = Array()){
        if (array_key_exists('email', $postData)) {
            $this->email = $postData['email'];
        }
        if (array_key_exists('password', $postData)) {
            $this->password = $postData['password'];
        }
        return $this;
    }//end of setData

    public function is_exist(){

        $query="SELECT * FROM `admin` WHERE  `email`=:email AND `password`=:password";
        $result=$this->DBH->prepare($query);
        $result->execute(array(':email'=>$this->email,':password'=>$this->password));

        $count = $result->rowCount();
        if ($count > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }//end of is_exist


    public function logged_in(){
        if ((array_key_exists('email', $_SESSION)) && (!empty($_SESSION['email']))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function log_out(){
        $_SESSION['email']="";
        return TRUE;
    }
}

