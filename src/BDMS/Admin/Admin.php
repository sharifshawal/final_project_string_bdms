<?php
namespace App\Admin;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;
use PDO;

class Admin extends DB
{
    public $table = "admin";
    public $id = "";
    public $admin_id = "";
    public $password = "";
    public $name = "";
    public $gender = "";
    public $email = "";
    public $mobile_number = "";
    public $organization = "";
    public $status = "";


    public function __construct(){
        parent::__construct();
    }


    public function setData($postData=array())
    {
        if (array_key_exists('id', $postData)) {
            $this->id = $postData['id'];
        }
        if (array_key_exists('admin_id', $postData)) {
            $this->admin_id= $postData['admin_id'];
        }
        if (array_key_exists('password', $postData)) {
            $this->password = $postData['password'];
        }

        if (array_key_exists('name', $postData)) {
            $this->name = $postData['name'];
        }
        if (array_key_exists('gender', $postData)) {
            $this->gender = $postData['gender'];
        }
        if (array_key_exists('email', $postData)) {
            $this->email = $postData['email'];
        }
        if (array_key_exists('mobile_number', $postData)) {
            $this->mobile_number = $postData['mobile_number'];
        }
        if (array_key_exists('organization', $postData)) {
            $this->organization = $postData['organization'];
        }
        if (array_key_exists('status', $postData)) {
            $this->status = $postData['status'];

        }
        return $this;
    }//end of setData

    public function store() {

        $query="INSERT INTO admin ('id', 'admin_id', 'password', 'name', 'gender', 'email', 'mobile_number', 'organization', 'status')
VALUES (:id, :admin_id, :password, :name, :gender, :email, :mobileNumber, :organization, :status)";

        $result=$this->DBH->prepare($query);

        $result->execute(array(':id'=>$this->id,
            ':admin_id'=>$this->admin_id,
            ':password'=>$this->password,
            ':name'=>$this->name,
            ':gender'=>$this->gender,
            ':email'=>$this->email,
            ':mobile_number'=>$this->mobile_number,
            ':organization'=>$this->organization,
            ':status'=>$this->status));

        if ($result) {
            Message::message("
                <div class=\"alert alert-success\">
                            <strong>Success!</strong> Data has been stored successfully, Please check your email and active your account.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        } else {
            Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Failed!</strong> Data has not been stored successfully.
                </div>");
            return Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }//end of store class

    public function change_password(){
        $query="UPDATE 'admin' SET 'password'=:password  WHERE 'email' =:email";
        $result=$this->DBH->prepare($query);
        $result->execute(array(':password'=>$this->password,':email'=>$this->email));

        if($result){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Password has been updated  successfully.
              </div>");
            // Utility::redirect('../../../../views//Profile/signUp.php');
        }
        else {
            echo "Error";
        }

    }//end of change password


    public function view(){
        $query="SELECT * FROM admin WHERE 'email' =:email";
        $result=$this->DBH->prepare($query);
        $result->execute(array(':email'=>$this->email));
        $row=$result->fetch(PDO::FETCH_OBJ);
        return $row;
    }// end of view()





    public function update(){

        $query="UPDATE admin SET  'name' =:name,  'mobile_number' =:mobile_number, 'organization' = :organization WHERE 'email' = :email";

        $result=$this->DBH->prepare($query);

        $result->execute(array( ':name'=>$this->name,
            ':mobile_number'=>$this->mobile_number,
            ':organization'=>$this->organization));

        if($result){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Data has been updated  successfully.
              </div>");
        }
        else {
            echo "Error";
        }
        return Utility::redirect($_SERVER['HTTP_REFERER']);
    }//e

    //////////////////////////////////
    ////////// Filter
    /////////        Method For User
    //////////////////////////////////

    public function filterData(){
        $sql="SELECT first_name, last_name, location, blood_group, mobile_number, email, number_visibilty FROM `bdms_user` WHERE `bdms_user`.location=:location AND `bdms_user`.blood_group=:blood_group";
        $result=$this->DBH->prepare($sql);
        $result->execute(array(':location'=>$this->location, ':blood_group'=>$this->blood_group));
        $allData=$result->fetchAll(PDO::FETCH_OBJ);
        return $allData;
    }// end of filterData()



}//end of Admin class

