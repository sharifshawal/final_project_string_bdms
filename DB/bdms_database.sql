-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2016 at 08:49 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bdms_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `admin_id` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile_number` varchar(20) NOT NULL,
  `organization` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `admin_id`, `password`, `name`, `gender`, `email`, `mobile_number`, `organization`, `status`) VALUES
(1, 'admin', '123456', 'Admin', 'Male', 'admin@admin.com', '010101010101', 'Admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bdms_user`
--

CREATE TABLE `bdms_user` (
  `id` int(20) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birthdate` date NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `last_donate_date` date NOT NULL,
  `location` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `email_verified` varchar(100) NOT NULL DEFAULT '0',
  `mobile_number` varchar(20) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `facebook_id` text NOT NULL,
  `number_visibilty` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Registered User Information';

--
-- Dumping data for table `bdms_user`
--

INSERT INTO `bdms_user` (`id`, `first_name`, `last_name`, `gender`, `birthdate`, `blood_group`, `last_donate_date`, `location`, `address`, `email`, `email_verified`, `mobile_number`, `user_id`, `password`, `facebook_id`, `number_visibilty`, `status`) VALUES
(1, 'Efthaqur', 'Alam', 'Male', '1992-01-01', 'B+', '2016-09-09', 'Chittagong', 'Old Chandgaon', 'efthaqur@gmail.com', 'Yes', '01816003456', 'efthaqur', '123456', 'fb.me/efthaqur', 1, 1),
(2, 'Istiyak', 'Amin', 'Male', '1999-12-03', 'A+', '2016-12-01', 'Chittagong', 'wireless', 'istiyakaminsanto@gmail.com', 'Yes', '01862864710', 'istiyak', '0000123', 'istiyakamin', 1, 1),
(3, 'Sumaiya ', 'Rahman', 'Female', '1992-12-28', 'A+', '2016-01-01', 'Chittagong', 'Chittagong Cantonment DOHS ', 'Sumaiya.c90s@gmail.com', 'Yes', '01779180185', 'Sumaiya rahman ', '@1212@', 'Sumaiya rahman ', 0, 1),
(4, 'Muhammad', 'Hasan', 'Male', '1990-02-01', 'B+', '0000-00-00', 'Chittagong', 'Chittagong', 'engr.hasan.apece1990@gmail.com', 'Yes', '01818960350', 'hasan', '123456', '', 0, 1),
(5, 'Tareq ', 'Aziz', 'Male', '1997-08-02', 'B+', '2016-11-11', 'Chittagong', 'Chittagong', 'aziztareq295@gmail.com', 'Yes', '01752224571', '', '123456', '', 0, 1),
(6, 'Arafat', 'Hossen', 'Male', '1993-07-05', 'O-', '0000-00-00', 'Chittagong', 'karerhat, chittagong', 'arafatmamun2021@gmail.com', 'Yes', '01853082442', 'arafat', '123456', '', 1, 1),
(7, 'Nafisa', 'Nasir', 'Female', '1997-10-22', 'B+', '0000-00-00', 'Chittagong', 'Chittagong', 'nafisanasir45@gmail.com', 'Yes', '01711371957', 'nafisanasir', '123456', '', 0, 1),
(8, 'M.I Shohelur', 'Rahman', 'Male', '1992-06-20', 'A+', '2016-10-25', 'Chittagong', 'Muradpur', 'sohelurrahman@gmail.com', 'Yes', '01777474846', 'Shohelur ', '000000', 'Not abailable', 1, 1),
(9, 'Ohedul', 'Islam', 'Male', '1995-10-10', 'O+', '0000-00-00', 'Chittagong', 'Not abailable', 'chuffobge43$gmail.com', 'Yes', '01811819725', 'jhdfjfgj', '000000', 'Not availeable', 1, 1),
(10, 'Ajit ', 'Dash', 'Male', '1990-04-25', 'A+', '2016-10-10', 'Chittagong', 'Chittagong', 'dasajit@gmail.com', 'Yes', '01843306208', 'Ajit', 'oooooo', 'AdamantAjit', 1, 1),
(11, 'Sayed', 'M.Ali', 'Male', '1991-10-20', 'AB+', '2016-12-01', 'Chakbajar', 'Chittagong', 'mdaliiiuc@gmail.com', 'Yes', '01878791819', 'Ali', '000000', 'md.Ali', 1, 1),
(12, 'Afsana', 'Ahmed', 'Female', '1995-06-08', 'B+', '0000-00-00', 'Chittagong', 'Chittagong', 'afsana@gamil.com', 'Yes', '01625452205', 'Afsana', '000000', 'Not Abaiable', 0, 1),
(13, 'Charles Valerio', 'Howlader', 'Male', '1992-04-14', 'B+', '2016-12-01', 'Chittagong', 'Chandgaon, Chittagong', 'charles.vh-14@live.com', 'Yes', '01670296443', 'cvhowlader', '123456', 'Charles Valerio Howlader', 1, 1),
(14, 'Ananda ', 'Das', 'Male', '1992-12-31', 'O+', '2016-06-25', 'Chittagong', 'Anderkilla,Chittagong', 'annadactg@gmail.com', 'Yes', '01837666661', 'A das', '12345', 'Ananda Ctg', 1, 1),
(15, 'Nandini Dutta', 'Mou', 'Female', '1994-10-08', 'A+', '2016-02-14', 'Chittagong', 'Butirygola,Chittagong', 'glraphical4200@gmail.com', 'Yes', '01819641436', 'nd Mou', '12345', 'Nandini Dutta Mou', 1, 1),
(16, 'Shweta Barua', 'Riya', 'Female', '1992-11-16', 'O+', '2016-03-16', 'Chittagong', 'Ctg Powerstation,Raojan,Chittagong', 'shwetariya16@gmail.com', 'Yes', '01878045546', 'SB riya', '12345', 'Rodela Riya', 1, 1),
(17, 'Antika', 'Saha', 'Female', '1994-07-01', 'B+', '2016-06-06', 'Chittagong', 'Colonelhat,Chittagong', 'blossomla6994@gmail.com', 'Yes', '01836769920', 'Antikasaha', '12345', 'Antika Saha', 1, 1),
(18, 'Mohammed', 'Hasan', 'Male', '1989-07-01', 'B+', '2016-01-01', 'Chittagong', 'Hathazari,Chittagong', 'mdhasan-ctgbd@yahoo.com', 'Yes', '01829618953', 'Mdhasan', '12345', 'Hasan', 1, 1),
(19, 'Fahima', 'Sultana', 'Female', '1995-11-30', 'O+', '2016-01-06', 'Chittagong', 'Sagorika Road,Chittagong', 'mermaid.princess30@gmail.com', 'Yes', '01626706941', 'Fsultana', '12345', 'Fahima sultana', 0, 1),
(20, 'Mardia Begum', 'Kashti', 'Female', '1996-11-03', 'B+', '2016-06-09', 'Chittagong', 'West Firarshah,Chittagong', 'mardia.kasti7@gmail.com', 'Yes', '01628098388', 'Mbkashti', '12345', 'Mardia Kashti', 0, 1),
(21, 'Md Faisal', 'Karim', 'Male', '1991-01-05', 'A+', '2015-12-01', 'Chittagong', 'Chawkbazar,Chittagong', 'faisalcse08@gmail.com', 'Yes', '018137129934', 'Fkarim', '12345', 'Faisal Karim', 1, 1),
(22, 'Rahad', 'Sikder', 'Male', '1993-09-19', 'A+', '2016-09-09', 'Chittagong', 'Citygate,Chittagong', 'rahadausthohin@gmail.com', 'Yes', '01838501803', 'Rsikder', '12345', 'Rahad Sikder', 1, 1),
(23, 'Avijit', 'Kar', 'Male', '1993-12-31', 'B+', '2016-01-01', 'Chittagong', 'Andorkilla,Chittagong', 'avijitkar117@gmail.com', 'Yes', '01821533828', 'Avijitkar', '12345', 'Avijit kar', 1, 1),
(24, 'Anik ', 'Anowar', 'Male', '1995-09-05', 'A+', '2016-06-06', 'Chittagong', 'Hillview,Chittagong', 'anrctg@gmail.com', 'Yes', '01818370509', 'Anik', '12345', 'Anowar Anik', 1, 1),
(25, 'Taher', 'Tawkir', 'Male', '1993-12-31', 'A+', '2016-04-09', 'Chittagong', 'O.R Nizam Road,Chittagong', 'thahertawkir7777@gmail.com', 'Yes', '01819307575', 'Taher', '12345', '', 1, 1),
(26, 'Mohammed', 'Younus', 'Male', '1993-02-01', 'O+', '2016-10-09', 'Chittagong', 'Greenview R/A,Pahartali,Chittagong', 'mohammed_younus@gmail.com', 'Yes', '01676132392', 'Mdyounus', '12345', 'Younus 19', 1, 1),
(27, 'Nazneen Sultana', 'Saimon', 'Female', '1993-02-28', 'B+', '0000-00-00', 'Chittagong', 'East Madar Bari Ctg', 'saimon1095@gmail.com', 'Yes', '01686489994', 'saimon1095', '123456', 'saimon9994', 1, 1),
(28, 'Naznin', 'Akter', 'Female', '1990-11-04', 'AB+', '0000-00-00', 'Chittagong', 'Muradpur, Ctg', 'akternaznin32@gmail.com', 'Yes', '01777508921', 'akternaznin32', '123456', '', 1, 1),
(29, 'Farzana', 'Hafsa', 'Female', '1992-01-06', 'B+', '2016-01-01', 'Chittagong', 'Halisahar, Chittagong', 'farzanahafsa.11@gmail.com', 'Yes', '01836418900', 'farzanahafsa.11', '123456', 'farzanahafsa.1', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shoutbox`
--

CREATE TABLE `shoutbox` (
  `id` int(20) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `post_date` date NOT NULL,
  `post_time` time NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='ShoutBox Table';

--
-- Dumping data for table `shoutbox`
--

INSERT INTO `shoutbox` (`id`, `user_id`, `message`, `post_date`, `post_time`, `status`) VALUES
(1, 'efthaqur', 'Efthi ( +8801816003456 ) from Chittagong needs B+ Blood emergency.', '2016-11-30', '02:09:00', 1),
(2, 'efthi', 'test', '0000-00-00', '00:00:00', 1),
(3, 'efthi1', 'test1', '0000-00-00', '00:00:00', 1),
(4, 'Efthi', 'Efthi ( +8801816003456 ) from Chittagong needs AB+ Blood emergency', '0000-00-00', '00:00:00', 1),
(5, 'Efthi', 'Efthi ( +8801816003456 ) from Chittagong needs AB+ Blood emergency', '2016-11-30', '02:33:09', 1),
(6, 'Istiyak', 'Istiyak ( +8801515286815 ) from Khulna needs O+ Blood emergency', '2016-11-30', '03:04:56', 1),
(7, 'Shawal', 'Shawal ( +8801815882438 ) from Rajshahi needs B- Blood emergency', '2016-11-30', '03:07:56', 1),
(8, 'Nazneen', 'Nazneen ( +88015678954 ) from Rajshahi needs AB- Blood emergency', '2016-11-30', '03:28:57', 1),
(9, 'Arman', 'Arman ( +8801711354561 ) from Shylet needs AB- Blood emergency', '2016-11-30', '08:25:58', 1),
(10, 'Lincoln', 'Lincoln ( +880174521356 ) from Dhaka needs O- Blood emergency', '2016-11-30', '08:27:42', 1),
(11, 'Ali', 'Ali ( +8801515445599 ) from Shylet needs AB- Blood emergency', '2016-12-02', '01:33:32', 1),
(12, 'Shila', 'Shila ( +880168085456 ) from Chittagong needs A- Blood emergency', '2016-12-02', '01:39:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `super_admin`
--

CREATE TABLE `super_admin` (
  `id` int(11) NOT NULL,
  `super_admin_id` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Super Admin Table';

--
-- Dumping data for table `super_admin`
--

INSERT INTO `super_admin` (`id`, `super_admin_id`, `password`, `email`, `status`) VALUES
(1, 'string_shawal', '123456', 'sharifshawal@yahoo.com', 1),
(2, 'string_efthaqur', '123456', 'efthaqur@outlook.com', 1),
(3, 'string_istiyak', '123456', 'istiyakaminsanto@gmail.com', 1),
(4, 'string_hafsa', '123456', 'farzanahafsa.11@gmail.com', 1),
(5, 'string_naznin', '123456', 'akternaznin32@gmail.com', 1),
(6, 'string_saimon', '123456', 'saimon1095@gmail.om', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bdms_user`
--
ALTER TABLE `bdms_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shoutbox`
--
ALTER TABLE `shoutbox`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `super_admin`
--
ALTER TABLE `super_admin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bdms_user`
--
ALTER TABLE `bdms_user`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `shoutbox`
--
ALTER TABLE `shoutbox`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `super_admin`
--
ALTER TABLE `super_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
