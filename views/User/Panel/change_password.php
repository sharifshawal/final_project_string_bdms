<?php
include_once('../../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;

use App\Utility\Utility;
use App\ShoutBox\ShoutBox;
use App\Message\Message;
if(!isset($_SESSION) )session_start();
$objShout = new ShoutBox();
$allData = $objShout->getData();

if(!isset($_SESSION) )session_start();

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();


if(!$status) {
    Utility::redirect('index.php');
    return;
}


?>

<!--/*-->
<!--include_once('../vendor/autoload.php');-->
<!--use App\User\User;-->
<!--use App\User\Auth;-->
<!--use App\Message\Message;-->
<!--use App\Utility\Utility;-->
<!---->
<!--$obj= new User();-->
<!--$obj->setData($_SESSION);-->
<!--$singleUser = $obj->view();-->
<!---->
<!--$auth= new Auth();-->
<!--$status = $auth->setData($_SESSION)->logged_in();-->
<!---->
<!--if(!$status) {-->
<!--    Utility::redirect('index.php');-->
<!--    return;-->
<!--}*/-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Blood Donor - Dashboard</title>

    <!-- Bootstrap core CSS -->

    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Merriweather|Montserrat|Shrikhand" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/dashboard.css">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top ">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">Blood Donor Information</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="../../find-donor.php"><span class="glyphicon glyphicon-search"></span> Find Donor</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../../about.php"><span class="glyphicon glyphicon-triangle-right"></span> Settings</a></li>
                <li><a href="../../contact.php"><span class="glyphicon glyphicon-triangle-right"></span>Profile</a></li>
                <li><a href="../Authentication/logout.php"><span class="glyphicon glyphicon-triangle-right"></span> Log Out!</a></li>
            </ul>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="dashboard.php">Dashboard <span class="sr-only">(current)</span></a></li>
                <li><a href="find-donor.php">Find Donor</a></li>
            </ul>
            <ul class="nav nav-sidebar">
                <li><a href="update_profile.php">Update Info</a></li>
                <li class="active"><a href="change_password.php">Change Password</a></li>
                <li><a href="../../help.php">Help</a></li>
                <li><a href="../../contact.php">Contact</a></li>
            </ul>

        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="row">

                <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!="") ?>
                <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                    echo "&nbsp;".Message::message();
                }
                Message::message(NULL);
                ?>
                <div class="container-fluid">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-info ">
                                    <div class="panel-heading">
                                        <h3 class="panel-title text-center">Change Password !</h3>
                                    </div>
                                    <div class="panel-body">
                                        <form class="form-horizontal" action="update.php" method="post">
                                            <fieldset>

                                                <!-- Form Name -->
                                                <legend>Change Password</legend>

                                                <!-- Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="password">Password</label>
                                                    <div class="col-md-4">
                                                        <input id="password" name="password" type="password" placeholder="Password" class="form-control input-md" required="">

                                                    </div>
                                                </div>

                                                <!-- Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="rtpassword">Repeat Password</label>
                                                    <div class="col-md-4">
                                                        <input id="rtpassword" name="rtpassword" type="password" placeholder=" Retype Password" class="form-control input-md" required="">

                                                    </div>
                                                </div>

                                                <div class="panel-footer">
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label" for="submit"></label>
                                                        <div class="col-md-4">
                                                            <input type="submit" name="submit" class="btn btn-lg btn-primary" value="Update">
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </form>
                                        <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( ".datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '1950:2016',
            dateFormat: 'yy-mm-dd',});
    } );
</script>

</body>
</html>
