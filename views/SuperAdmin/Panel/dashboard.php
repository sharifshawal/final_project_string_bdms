<?php
include_once('../../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;

use App\Utility\Utility;
use App\ShoutBox\ShoutBox;
use App\Message\Message;
if(!isset($_SESSION) )session_start();
$objShout = new ShoutBox();
$allData = $objShout->getData();

if(!isset($_SESSION) )session_start();

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();



if(!$status) {
    Utility::redirect('index.php');
    return;
}
?>

<!--/*-->
<!--include_once('../vendor/autoload.php');-->
<!--use App\User\User;-->
<!--use App\User\Auth;-->
<!--use App\Message\Message;-->
<!--use App\Utility\Utility;-->
<!---->
<!--$obj= new User();-->
<!--$obj->setData($_SESSION);-->
<!--$singleUser = $obj->view();-->
<!---->
<!--$auth= new Auth();-->
<!--$status = $auth->setData($_SESSION)->logged_in();-->
<!---->
<!--if(!$status) {-->
<!--    Utility::redirect('index.php');-->
<!--    return;-->
<!--}*/-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Blood Donor - Dashboard</title>

    <!-- Bootstrap core CSS -->

    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Merriweather|Montserrat|Shrikhand" rel="stylesheet">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/dashboard.css">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top ">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../../index.php">Blood Donor Information</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="find-donor.php"><span class="glyphicon glyphicon-search"></span> Find Donor</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../../about.php"><span class="glyphicon glyphicon-triangle-right"></span> Settings</a></li>
                <li><a href="user_profile.php"><span class="glyphicon glyphicon-triangle-right"></span>Profile</a></li>
                <li><a href="../../Authentication/logout.php"><span class="glyphicon glyphicon-triangle-right"></span> Log Out!</a></li>
            </ul>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li class="active"><a href=dashboard.php>Dashboard <span class="sr-only">(current)</span></a></li>
                </ul>
                <ul class="nav nav-sidebar">
                    <hr>
                    <li><a href="create_admin.php">Create Admin</a></li>
                    <hr>
                </ul>
                <ul class="nav nav-sidebar">
                    <li><a href="#">Help</a></li>
                    <li><a href="#">Contact</a></li>
                </ul>

            </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="row">

                <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!="") ?>
                <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                    echo "&nbsp;".Message::message();
                }
                Message::message(NULL);
                ?>
                <div class="container-fluid">
                    <div class="col-md-4">
                        <div class="panel panel-danger">
                            <div class="panel-heading"><h3 class="panel-title"><strong>Find Donor </strong></h3></div>
                            <div class="panel-body">
                                <form role="form" action="search_result.php" method="post">

                                    <!-- Select Location -->
                                    <div class="form-group">
                                        <label class="control-label" for="location">Location</label>
                                        <div class="">
                                            <select id="location" name="location" class="form-control input-md">
                                                <option value="Chittagong">Chittagong</option>
                                                <option value="Dhaka">Dhaka</option>
                                                <option value="Khulna">Khulna</option>
                                                <option value="Rajshahi">Rajshahi</option>
                                                <option value="Shylet">Shylet</option>
                                            </select>
                                        </div>
                                    </div>
                                    <!-- Select Blood Group -->
                                    <div class="form-group">
                                        <label class=" control-label" for="blood_group">Blood Group</label>
                                        <div class="">
                                            <select id="blood_group" name="blood_group" class="form-control input-md">
                                                <option>A+ </option>
                                                <option>A-</option>
                                                <option>AB+</option>
                                                <option>AB-</option>
                                                <option>B+</option>
                                                <option>B-</option>
                                                <option>O+</option>
                                                <option>O-</option>
                                            </select>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-lg btn-danger">Find Blood Donor</button>

                                </form>
                            </div>
                        </div>
                        <div class="panel panel-info">
                            <div class="panel-heading"><h3 class="panel-title"><strong>User Info</strong></h3></div>
                            <div class="panel-body">
                                <?php
                                /**
                                 * Show user information like IP address, useragent
                                 **/
                                $ip = $_SERVER['REMOTE_ADDR'];
                                $browser = $_SERVER['HTTP_USER_AGENT'];

                                echo "<b>Visitor IP address:</b><br/>" . $ip . "<br/>";
                                echo "<b>Browser (User Agent) Info:</b><br/>" . $browser . "<br/>";

                                echo "<b>Login Email:</b><br/>".$_SESSION['email']."<br>";

                                echo "<b>Last Blood Donate Date:</b> <br>";
                                echo $singleUser->last_donate_date;


                                ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-chat shoutbox">
                                    <div class="panel-heading">
                                        <h3 class="panel-title text-center">Shout!</h3>
                                    </div>
                                    <div class="chat-messages">
                                        <ul class="list-group">
                                            <?php
                                            $row =1;

                                            foreach ($allData as $singleData){
                                                if($row%2==0) $listStyle="list-group-item-danger";
                                                else $listStyle="list-group-item-info";
                                                echo "<li class='list-group-item $listStyle'>";
                                                echo "$singleData->post_date"." "."<time>$singleData->post_time</time>"." ".$singleData->message;
                                                echo "</li>";
                                                $row++;
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="panel-footer ">
                                        <div class="form-group">
                                            <form class="form-horizontal" action="../../shout_post.php" method="post">
                                                <div class="form-group">
                                                    <label for="display_name" class="col-sm-2 control-label">Name</label>
                                                    <div class="col-sm-3">
                                                        <input type="tel" class="form-control" id="display_name" name="display_name" placeholder="Name" required>
                                                    </div>
                                                    <label for="mobile_number" class="col-sm-2 control-label">Mobile Number</label>
                                                    <div class="col-sm-3">
                                                        <input type="tel" class="form-control" id="mobile_number" name="mobile_number" placeholder="Mobile Number" required>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label" for="location">Select Location</label>
                                                    <div class="col-sm-3">
                                                        <select id="location" name="location" class="form-control ">
                                                            <option value="Chittagong">Chittagong</option>
                                                            <option value="Dhaka">Dhaka</option>
                                                            <option value="Khulna">Khulna</option>
                                                            <option value="Rajshahi">Rajshahi</option>
                                                            <option value="Shylet">Shylet</option>
                                                        </select>
                                                    </div>
                                                    <label  class="col-sm-2 control-label" for="blood_group">Blood Group</label>
                                                    <div class="col-sm-2">
                                                        <select id="blood_group" name="blood_group" class="form-control">
                                                            <option>A+ </option>
                                                            <option>A-</option>
                                                            <option>AB+</option>
                                                            <option>AB-</option>
                                                            <option>B+</option>
                                                            <option>B-</option>
                                                            <option>O+</option>
                                                            <option>O-</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-3">
                                                        <button type="submit" class="btn btn-primary">Send Shout!</button>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <button type="reset" class="btn btn-warning">Reset</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <p id="chat-error" class="hidden text-danger"></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>


</body>
</html>
