<?php

include_once('../../../vendor/autoload.php');
use App\Admin\Auth;
use App\Message\Message;
use App\Utility\Utility;

if(!isset($_SESSION))session_start();
$auth= new Auth();
$auth->setData($_POST);  // this prepare() is  equivalent to setData method
$status= $auth->is_exist();
{

    if ($status) {
        $_SESSION['email'] = $_POST['email'];


        Message::message("
                <div class=\"alert alert-success\">
                            <strong>Welcome!</strong> You have successfully logged in.
                </div>");

        return Utility::redirect('../Panel/dashboard.php');

    } else {
        Message::message("
                <div class=\"alert alert-danger\">
                            <strong>Wrong information!</strong> Please try again.
                </div>");

        return Utility::redirect('../../admin-login.php');

    }


}
/*{
    if ($_SESSION["logged"]) // if already logged, redirect to admin page
        return Utility::redirect('../Panel/dashboard.php');
    else
        return Utility::redirect('../../admin-login.php');
}*/
?>