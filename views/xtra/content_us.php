<!DOCTYPE html>
<html lang="en" class="no-js" >
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <title>Blood Doner Information</title>
    <!-- BOOTSTRAP CORE CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- ION ICONS STYLES -->
    <link href="assets/css/ionicons.css" rel="stylesheet" />
    <!-- FONT AWESOME ICONS STYLES -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- FANCYBOX POPUP STYLES -->
    <link href="assets/js/source/jquery.fancybox.css" rel="stylesheet" />
    <!-- STYLES FOR VIEWPORT ANIMATION -->
    <link href="assets/css/animations.min.css" rel="stylesheet" />
    <!-- CUSTOM CSS -->
    <link href="assets/css/content.css" rel="stylesheet" />
    <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body data-spy="scroll" data-target="#menu-section">
<!--MENU SECTION START-->
<div class="navbar navbar-inverse navbar-fixed-top scroll-me" id="menu-section" >
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">

                Blood Doner Information

            </a>

            <li><a href="#contact">CONTACT</a></li>
            </ul>
        </div>

    </div>
</div>
<!--MENU SECTION END-->
<!--CONTACT SECTION START-->
<section id="contact" >
    <div class="container">
        <div class="row text-center header animate-in" data-anim-type="fade-in-up">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <h3>Contact Details </h3>
                <hr />

            </div>
        </div>

        <div class="row animate-in" data-anim-type="fade-in-up">

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="contact-wrapper">
                    <h3>Efthaqur Alam Efthi</h3>
                    <h4><strong>Email : </strong> efthaqur@gmail.com </h4>
                    <h4><strong>Call : </strong> 01720000000 </h4>
                    <h4><strong>Skype : </strong> efthi </h4>
                    <h4><strong>Address:</strong> chadgao, Chittagong</h4>

                    <div class="social-below">
                        <a href="https://www.facebook.com/efthaqur" class="btn button-custom btn-custom-two" > Facebook</a>
                        <a href="https://plus.google.com/+EfthaqurAlamEfthi" class="btn button-custom btn-custom-two" > Google +</a>

                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="contact-wrapper">
                    <h3>Sharif Mahmud Shawal</h3>
                    <h4><strong>Email : </strong> sharifshawal@yahoo.com </h4>
                    <h4><strong>Call : </strong> 0172000000 </h4>
                    <h4><strong>Skype : </strong> Sharif Shawal </h4>
                    <h4><strong>Address:</strong> 2no gate, Chittagong </h4>

                    <div class="social-below">
                        <a href="https://www.facebook.com/sharif.shawal" class="btn button-custom btn-custom-two" > Facebook</a>
                        <a href="https://mail.google.com/mail/u/0/?view=cm&tf=1&fs=1&to=%22Sharif%20Shawal%22%20%3Csharifshawal%40yahoo.com%3E" class="btn button-custom btn-custom-two" > Google +</a>

                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="contact-wrapper">
                    <h3>Istiyak Amin Shanto</h3>
                    <h4><strong>Email : </strong> info@yuordomain.com </h4>
                    <h4><strong>Call : </strong> 0172000000 </h4>
                    <h4><strong>Skype : </strong> shanto </h4>
                    <h4><strong>Address:</strong> GEC, Chittagong</h4>
                    <div class="social-below">
                        <a href="https://www.facebook.com/istiyakaminsanto" class="btn button-custom btn-custom-two" > Facebook</a>
                        <a href="#" class="btn button-custom btn-custom-two" > Google +</a>

                    </div>
                </div>
            </div>

        </div>

        <!--second part--->

        <div class="row animate-in" data-anim-type="fade-in-up">

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="contact-wrapper">
                    <h3>Farzana Hafsa </h3>
                    <h4><strong>Email : </strong> farzanahafsa.11@gmail.com </h4>
                    <h4><strong>Call : </strong> 0172000000 </h4>
                    <h4><strong>Skype : </strong> hafsa </h4>
                    <h4><strong>Address:</strong> Halishahar, Chittagong</h4>

                    <div class="social-below">
                        <a href="https://www.facebook.com/farzana.hafsa1" class="btn button-custom btn-custom-two" > Facebook</a>
                        <a href="#" class="btn button-custom btn-custom-two" > Google +</a>

                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="contact-wrapper">
                    <h3>Shila Naznin</h3>
                    <h4><strong>Email : </strong> akthernaznin32@gmail.com </h4>
                    <h4><strong>Call : </strong> 0172000000 </h4>
                    <h4><strong>Skype : </strong> naznin </h4>
                    <h4><strong>Address:</strong> Murad, Chittagong</h4>

                    <div class="social-below">
                        <a href="https://www.facebook.com/shila.naznin.96" class="btn button-custom btn-custom-two" > Facebook</a>
                        <a href="#" class="btn button-custom btn-custom-two" > Google +</a>

                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="contact-wrapper">
                    <h3>Nazneen Sultana Naznin</h3>
                    <h4><strong>Email : </strong> nazneensultana@gmail.com </h4>
                    <h4><strong>Call : </strong> 0172000000 </h4>
                    <h4><strong>Skype : </strong> naznin </h4>
                    <h4><strong>Address:</strong> Agrabad, Chittagong</h4>
                    <div class="social-below">
                        <a href="https://www.facebook.com/saimon9994" class="btn button-custom btn-custom-two" > Facebook</a>
                        <a href="#" class="btn button-custom btn-custom-two" > Google +</a>

                    </div>
                </div>
            </div>
        </div>

    </div>


</section>
<!--CONTACT SECTION END-->

<!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME -->
<!-- CORE JQUERY -->
<script src="assets/js/jquery-1.11.1.js"></script>
<!-- BOOTSTRAP SCRIPTS -->
<script src="assets/js/bootstrap.js"></script>
<!-- EASING SCROLL SCRIPTS PLUGIN -->
<script src="assets/js/vegas/jquery.vegas.min.js"></script>
<!-- VEGAS SLIDESHOW SCRIPTS -->
<script src="assets/js/jquery.easing.min.js"></script>
<!-- FANCYBOX PLUGIN -->
<script src="assets/js/source/jquery.fancybox.js"></script>
<!-- ISOTOPE SCRIPTS -->
<script src="assets/js/jquery.isotope.js"></script>
<!-- VIEWPORT ANIMATION SCRIPTS   -->
<script src="assets/js/appear.min.js"></script>
<script src="assets/js/animations.min.js"></script>
<!-- CUSTOM SCRIPTS -->
<script src="assets/js/custom.js"></script>
</body>

</html>
