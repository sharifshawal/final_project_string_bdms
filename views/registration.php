<?php
include_once('../vendor/autoload.php');
use App\Message\Message;
if(!isset($_SESSION) )session_start();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Blood Donor - Register</title>

    <!-- Bootstrap core CSS -->

    <link rel="stylesheet" href="../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Merriweather|Montserrat|Shrikhand" rel="stylesheet">
    <link rel="stylesheet" href="../resource/assets/bootstrap/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top ">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Blood Donor Information</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                <li><a href="find-donor.php"><span class="glyphicon glyphicon-search"></span> Find Donor</a></li>
                <li><a href="terms.php"><span class="glyphicon glyphicon-asterisk"></span> Term &amp; Privacy</a></li>
                <li><a href="registration.php"><span class="glyphicon glyphicon-search"></span> Sign Up!</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="admin-login.php"><span class="glyphicon glyphicon-user"></span> Admin</a></li>
                <li><a href="about.php"><span class="glyphicon glyphicon-triangle-right"></span> About Us</a></li>
                <li><a href="contact.php"><span class="glyphicon glyphicon-triangle-right"></span> Contact Us</a></li>
                <li><a href="help.php"><span class="glyphicon glyphicon-triangle-right"></span> Help</a></li>
            </ul>
        </div><!--/.navbar-collapse -->
    </div>
</nav>


<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>Donate Blood, Save a Life!</h1>
                <p class="header-text">Location Specfic Blood Donor Information,
                    <br> Search your nearest Blood Donor! </p>

            </div>
            <div class="col-md-4">
                <img src="../resource/assets/img/logo-2.png" alt="" class="img-responsive">
            </div>
        </div>


    </div>

</div>
<div class="container">
    <div class="masthead">
        <nav>
            <ul class="nav nav-justified">
                <li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                <li class="active"><a href="registration.php"><span class="glyphicon glyphicon-triangle-right"></span> Register</a></li>
                <li><a href="find-donor.php"><span class="glyphicon glyphicon-search"></span> Find Donor</a></li>
                <li><a href="objective.php"><span class="glyphicon glyphicon-triangle-right"></span> Our Objectives</a></li>
                <li><a href="about.php"><span class="glyphicon glyphicon-triangle-right"></span> About Us</a></li>
                <li><a href="contact.php"><span class="glyphicon glyphicon-triangle-right"></span> Contact</a></li>
            </ul>
        </nav>
    </div>
    <hr>
</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="container">
            <div class="col-md-8 center-block">
                <div class="panel panel-info">
                    <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!="") ?>
                    <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                        echo "&nbsp;".Message::message();
                    }
                    Message::message(NULL);
                    ?>
                    <div class="panel-heading"><h3 class="panel-title"><strong>Register</strong></h3></div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="Profile/registration.php" method="post">
                            <fieldset>

                                <!-- Form Name -->
                                <legend>Personal Information</legend>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="first_name">First name</label>
                                    <div class="col-md-4">
                                        <input id="first_name" name="first_name" type="text" placeholder="First Name" class="form-control input-md" required="">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="last_name">Last name</label>
                                    <div class="col-md-4">
                                        <input id="last_name" name="last_name" type="text" placeholder="Last Name" class="form-control input-md" required="">

                                    </div>
                                </div>
                                <!-- Multiple Radios (inline) -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="gender">Gender</label>
                                    <div class="col-md-4 radio" >
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" id="inlineRadio1" value="Male"> Male
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="gender" id="inlineRadio2" value="Female"> Female
                                        </label>
                                    </div>
                                </div>
                                <!-- Date of Birth input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="birthdate">Birthdate</label>
                                    <div class="col-md-4">
                                        <input id="birthdate" name="birthdate" type="text" placeholder="Birthdate" class="form-control input-md datepicker" required="">
                                    </div>
                                </div>
                                <!-- Select Blood Group -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="blood_group">Blood Group</label>
                                    <div class="col-md-4">
                                        <select id="blood_group" name="blood_group" class="form-control input-md">
                                            <option>A+ </option>
                                            <option>A-</option>
                                            <option>AB+</option>
                                            <option>AB-</option>
                                            <option>B+</option>
                                            <option>B-</option>
                                            <option>O+</option>
                                            <option>O-</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Last Blood Donate Date input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="last_donate_date">Last Blood Donate Date</label>
                                    <div class="col-md-4">
                                        <input id="last_donate_date" name="last_donate_date" type="text" placeholder="Last Blood Donate Date" class="form-control input-md datepicker" >
                                    </div>
                                </div>
                                <!-- Select Location -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="location">Location</label>
                                    <div class="col-md-4">
                                        <select id="location" name="location" class="form-control input-md">
                                            <option value="Chittagong">Chittagong</option>
                                            <option value="Dhaka">Dhaka</option>
                                            <option value="Khulna">Khulna</option>
                                            <option value="Rajshahi">Rajshahi</option>
                                            <option value="Shylet">Shylet</option>
                                        </select>
                                    </div>
                                </div>

                                <legend>Contact Information</legend>
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="address">Address </label>
                                    <div class="col-md-4">
                                        <input id="address" name="address" type="text" placeholder="Address" class="form-control input-md" required="">

                                    </div>
                                </div>


                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="email">Email</label>
                                    <div class="col-md-4">
                                        <input id="email" name="email" type="text" placeholder="Email Address" class="form-control input-md" required="">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="facebook_id">Facebook ID</label>
                                    <div class="col-md-4">
                                        <input id="facebook_id" name="facebook_id" type="text" placeholder="Facebook ID" class="form-control input-md">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="mobile_number">Mobile Number</label>
                                    <div class="col-md-4">
                                        <input id=mobile_number" name="mobile_number" type="text" placeholder="Mobile number" class="form-control input-md" required="">

                                    </div>
                                </div>

                                <!-- Multiple Radios (inline) -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="number_visibilty">Do you want to visible your Mobile Number Publicly?</label>
                                    <div class="col-md-4 radio" >
                                        <label class="radio-inline">
                                            <input type="radio" name="number_visibilty" id="inlineRadio1" value="1"> Yes
                                        </label>
                                        <label class="radio-inline">
                                            <input type="radio" name="number_visibilty" id="inlineRadio2" value="0"> No
                                        </label>
                                    </div>
                                </div>


                                <legend>Login Information</legend>
                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="username">Username</label>
                                    <div class="col-md-4">
                                        <input id="username" name="username" type="text" placeholder="Username" class="form-control input-md" required="">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="password">Password</label>
                                    <div class="col-md-4">
                                        <input id="password" name="password" type="password" placeholder="Password" class="form-control input-md" required="">

                                    </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="rtpassword">Repeat Password</label>
                                    <div class="col-md-4">
                                        <input id="rtpassword" name="rtpassword" type="password" placeholder=" Retype Password" class="form-control input-md" required="">

                                    </div>
                                </div>




                                <!-- Button -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="submit"></label>
                                    <div class="col-md-4">
                                        <input type="submit" name="submit" class="btn btn-primary" value="Register">
                                        <input type="reset" name="reset" class="btn btn-primary" value="Reset">
                                    </div>
                                </div>

                            </fieldset>
                        </form>
                        <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
                    </div>
                </div>
            </div>


        </div>
        <hr>

        <hr>

        <footer>
            <p>&copy; 2016 Blood Donor Information System. Non Profit Organization.</p>
        </footer>
        <hr>
    </div>
</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- Javascript -->
<script src="../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
    $( function() {
        $( ".datepicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '1950:2016',
            dateFormat: 'yy-mm-dd',});
    } );
</script>
<script>
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });
</script>



</body>
</html>
