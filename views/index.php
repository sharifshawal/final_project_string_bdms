<?php
include_once('../vendor/autoload.php');

if(!isset($_SESSION) )session_start();
use App\Message\Message;
use App\ShoutBox\ShoutBox;

$objShout = new ShoutBox();
$allData = $objShout->getData();

?>




<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Blood Donor - Homepage</title>

    <!-- Bootstrap core CSS -->

    <link rel="stylesheet" href="../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../resource/assets/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lobster|Merriweather|Montserrat|Shrikhand" rel="stylesheet">
    <link rel="stylesheet" href="../resource/assets/bootstrap/css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top ">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Blood Donor Information</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
                <li><a href="index.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                <li><a href="find-donor.php"><span class="glyphicon glyphicon-search"></span> Find Donor</a></li>
                <li><a href="terms.php"><span class="glyphicon glyphicon-asterisk"></span> Term &amp; Privacy</a></li>
                <li><a href="registration.php"><span class="glyphicon glyphicon-search"></span> Sign Up!</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="admin-login.php"><span class="glyphicon glyphicon-user"></span> Admin</a></li>
                <li><a href="xtra/about_us.html"><span class="glyphicon glyphicon-triangle-right"></span> About Us</a></li>
                <li><a href="xtra/contact_us.html"><span class="glyphicon glyphicon-triangle-right"></span> Contact Us</a></li>
                <li><a href="help.php"><span class="glyphicon glyphicon-triangle-right"></span> Help</a></li>
            </ul>
        </div><!--/.navbar-collapse -->
    </div>
</nav>

<!-- Main jumbotron for a primary marketing message or call to action -->
<div class="jumbotron">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h1>Donate Blood, Save a Life!</h1>
                <p class="header-text">Location Specfic Blood Donor Information,
                    <br> Search your nearest Blood Donor! </p>

            </div>
            <div class="col-md-4">
                <img src="../resource/assets/img/logo-2.png" alt="" class="img-responsive">
            </div>
        </div>


    </div>

</div>
<div class="container">
    <div class="masthead">
        <nav>
            <ul class="nav nav-justified">
                <li class="active"><a href="#"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                <li><a href="registration.php"><span class="glyphicon glyphicon-triangle-right"></span> Register</a></li>
                <li><a href="find-donor.php"><span class="glyphicon glyphicon-search"></span> Find Donor</a></li>
                <li><a href="objective.php"><span class="glyphicon glyphicon-triangle-right"></span> Our Objectives</a></li>
                <li><a href="about.php"><span class="glyphicon glyphicon-triangle-right"></span> About Us</a></li>
                <li><a href="contact.php"><span class="glyphicon glyphicon-triangle-right"></span> Contact</a></li>
            </ul>
        </nav>
    </div>
    <hr>
</div>
<div class="row">
    <div class="container-fluid ">
        <div class="col-md-4 col-md-offset-4 text-center">
            <?php  if(isset($_SESSION['message']) )if($_SESSION['message']!="") ?>
            <?php if((array_key_exists('message',$_SESSION)&& (!empty($_SESSION['message'])))) {
                echo "&nbsp;".Message::message();
            }
            Message::message(NULL);
            ?>
        </div>
    </div>
</div>


<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="container">
            <div class="col-md-4">
                <div class="panel panel-danger">
                    <div class="panel-heading"><h3 class="panel-title"><strong>Sign In </strong></h3></div>
                    <div class="panel-body">
                        <?php
                            if (empty($_SESSION['email'])){
                        ?>
                        <form role="form" action="User/Authentication/login.php" method="post">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Username or Email</label>
                                <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Password <a href="/sessions/forgot_password">(forgot password)</a></label>
                                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                            </div>
                            <button type="submit" class="btn btn-primary">Sign in</button>
                            <a href="registration.php" class="btn btn-danger">Register</a>
                        </form>
                        <?php
                                }
                            else {

                                ?>
                                <p class="alert alert-info alert-link "> You have been logged in.
                                    <br>
                                    <a href="User/Panel/dashboard.php" class="alert-link">Click Here</a> to go in Dashboard!
                                </p>
                             <?php
                                }
                            ?>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-chat shoutbox">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">Shout!</h3>
                            </div>
                            <div class="chat-messages">
                                <ul class="list-group">
                                    <?php
                                    $row =1;

                                    foreach ($allData as $singleData){
                                        if($row%2==0) $listStyle="list-group-item-info";
                                        else $listStyle="list-group-item-warning";
                                        echo "<li class='list-group-item $listStyle'>";
                                        echo "$singleData->post_date"." "."<time>$singleData->post_time</time>"." ".$singleData->message;
                                        echo "</li>";
                                        $row++;
                                    }
                                    ?>
                                </ul>
                            </div>
                            <div class="panel-footer ">
                                <div class="alert alert-info" role="alert">Log in or Sign Up to Shout! You can Look for desired blood group Donor by sending Shouts!</div>
                            </div>
                        </div>
                    </div>
                </div>
                <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-chevron-up"></span></a>
            </div>
        </div>
        <hr>
        <div class="container table-background">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <h4 class="blood-type">Human Blood Type Chart </h4>
                </div>
            </div>
            <div class="row transparent-bg">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table  text-center">
                            <thead>
                            <tr>
                                <th>ABO Blood Type</th>
                                <th>Donor</th>
                                <th>Receiver</th>
                                <th>General Population</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>O+</td>
                                <td>O+, A+, B+, AB+</td>
                                <td>O+, O- </td>
                                <td>38.5</td>
                            </tr>
                            <tr>
                                <td>O-</td>
                                <td>Universal Donor </td>
                                <td>O-</td>
                                <td>6.5</td>
                            </tr>
                            <tr>
                                <td>A+</td>
                                <td>A+, AB+ </td>
                                <td>A+, A-, O+, O-</td>
                                <td>34.3</td>
                            </tr>
                            <tr>
                                <td>A-</td>
                                <td>A-, A+, AB-, AB+</td>
                                <td>A-, O- </td>
                                <td>5.7</td>
                            </tr>
                            <tr>
                                <td>B+</td>
                                <td>B+, AB+ </td>
                                <td>B+, B-, O+, O-</td>
                                <td>8.6</td>
                            </tr>
                            <tr>
                                <td>B-</td>
                                <td>B-, B+, AB-, AB+</td>
                                <td>B-, O-</td>
                                <td>1.7</td>
                            </tr>
                            <tr>
                                <td>AB+</td>
                                <td>AB+</td>
                                <td>Universal Receiver</td>
                                <td>4.3</td>
                            </tr>
                            <tr>
                                <td>AB-</td>
                                <td>AB-, AB+</td>
                                <td>AB-, A-, B-, O-</td>
                                <td>0.7</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><br>
        </div>
        <hr>

        <footer>
            <p>&copy; 2016 Blood Donor Information System. Non Profit Organization.</p>
        </footer>
        <hr>
    </div>
</div> <!-- /container -->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- Javascript -->
<script src="../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function(){
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');

    });
</script>



</body>
</html>
