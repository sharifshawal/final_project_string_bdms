Blood Donor Management/Information System (BDMS/BDIS)
=======

> Written by [Sharif Mahmud Shawal]

**Intro**
 Location Specific Blood Donor information storing and providing to user so that he/she can find out nearest blood donor and contact on emergency.




**Directory Map**
   

    ├──Project Directory /
        │   
        ├── DB/
        │   ├── Database Structure File
        │   
        ├── resource/
        │   ├── assets
        │       ├── bootstrap
        │       ├── font-awesome
        │       ├── google-fonts
        │       ├── img
        │       ├── js (3rd Party)
        │
        ├── src/
        │   ├── Class Files
        │   
        ├── vendor/
        │   ├── 3rd Party Vendor Files
        │   
        └── views/
            ├── Public Files

